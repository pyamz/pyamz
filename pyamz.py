#! /usr/bin/env python

# Python .amz Amazon MP3 file handling library/script
# by Sam Lade, 2011
# This code is public domain.
__version__ = "0.0.3"
import re

def hex_to_str(key):
    """Convert a hexadecimal key to a string usable by the DES functions"""
    return "".join(chr(int(key[i:i+2], 16)) for i in range(0,len(key), 2))

def decrypt(b64):
    """If necessary, decrypt the supplied AMZ file contents"""
    if not b64.startswith(("<?xml", "\n<playlist")):
        # this is an encrypted amz
        try:
            # fast DES algorithm
            from Crypto.Cipher.DES import new as des, MODE_CBC as CBC
        except ImportError:
            # slow pure Python DES algorithm, easier to deploy
            from pyDes import des, CBC
        import base64

        encrypted = base64.b64decode(b64)

        d = des(hex_to_str("29AB9D18B2449E31"), mode=CBC, IV=hex_to_str("5E72D79A11B34FEE"))
        return d.decrypt(encrypted)
    else:
        return b64

def parse(xml):
    """Parse the XML from an AMZ file into a list of dicts of track info"""
    from lxml import etree

    # check for padding
    pad = xml.rindex(">")
    tree = etree.XML(xml[:pad+1])
    # define shorthand namespace for use in xpaths
    nsdef = "http://xspf.org/ns/0/"
    nsdict = {"a":nsdef}
    if nsdef in xml:
        ns = "a:"
    else:
        ns = ""
    trackxml = tree.xpath("//{0}trackList/{0}track".format(ns), namespaces=nsdict)
    tracks = []

    # standard XSPF fields
    fields = {"title":"title", "album":"album", "creator":"artist", "trackNum":"track", "location":"url"}
    # extra non-standard metadata supplied as <meta rel=...> tags
    metafields = {"primaryGenre":"genre", "discNum":"disk", "albumPrimaryArtist":"albumartist"}
    for i in trackxml:
        trackinfo = {}
        for j in fields:
            try:
                trackinfo[fields[j]] = i.xpath("{}{}".format(ns, j), namespaces=nsdict)[0].text
            except IndexError:
                trackinfo[fields[j]] = ""
        for j in metafields:
            try:
                trackinfo[metafields[j]] = i.xpath("{}meta[@rel='http://www.amazon.com/dmusic/{}']".format(ns, j), namespaces=nsdict)[0].text
            except IndexError:
                trackinfo[metafields[j]] = ""
        tracks.append(trackinfo)
    return tracks

def filename_safe(path_component):
    return re.sub(r"["+re.escape(r'"*/:<>?\|[]')+"]", "_", path_component)

def filename_safe_dict(items, all_chars):
    for i in items:
        if all_chars:
            items[i] = filename_safe(items[i])
        else:
            items[i] = items[i].replace("/", "_")
    return items

if __name__ == "__main__":
    import subprocess, os, argparse, urllib2, platform

    parser = argparse.ArgumentParser(description="Parse .amz files from Amazon MP3")
    parser.add_argument("amz", metavar="FILE", help="An .amz file to open")
    parser.add_argument("-n", "--name", default="{albumartist}/{album}/{track:0>2} {title}", help="File naming string. Available fields: {track} {title} {album} {artist} {genre} {disk} {albumartist}. Default: {albumartist}/{album}/{track:0>2} {title}")
    if platform.system() != "Windows":
        parser.add_argument("-e", "--sanitise", default=False, action="store_true", help="Remove characters which are banned on some filesystems from paths before saving.")
    dlgroup = parser.add_mutually_exclusive_group()
    dlgroup.add_argument("-d", "--decrypt-only", default=False, action="store_true", help="Print the decrypted XML to stdout and exit")
    dlgroup.add_argument("-w", "--wget", default=False, action="store_true", help="Use external wget to download files")
    dlgroup.add_argument("-s", "--simulate", default=False, action="store_true", help="Simulate downloading and print URLs, but don't actually download anything.")
    parser.add_argument("--version", action="version", version="PyAmz " + __version__)
    args = parser.parse_args()

    b64 = open(args.amz).read()
    try:
        xml = decrypt(b64)
    except ImportError:
        print "One of PyCrypto or pyDes is required to handle encrypted AMZ files."
        exit()

    if args.decrypt_only:
        print xml
        exit()

    try:
        tracks = parse(xml)
    except ImportError:
        print "lxml is required to parse the XML contents of decrypted AMZ files."

    if platform.system() == "Windows":
        sanitise = True
    else:
        sanitise = args.sanitise

    for i in tracks:
        url = i["url"]
        print u"Downloading {title} - {artist} ({i}/{n})...".format(i=tracks.index(i)+1, n=len(tracks), **i)
        filename = unicode(args.name).format(**filename_safe_dict(i, sanitise)) + ".mp3"
        path = os.path.split(filename)[0]
        if not os.path.exists(path):
            os.makedirs(path)
        if args.wget:
            subprocess.Popen(("wget", "-O", filename, url)).communicate()
        elif args.simulate:
            print url, "-->", filename
        else:
            with open(filename, "wb") as f:
                f.write(urllib2.urlopen(url).read())
